import React, { Component, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet
} from "react-native";
import Navigation from "./src/Navigation";
import { bgrunner } from './src/utils/background'

export default function App() {

  useEffect(() => {

    bgrunner.checksystem()


  }, [])


  return (

    <Navigation />

  );

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});