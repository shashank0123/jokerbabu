export default {
  p1: require('../assets/Images/1.jpg'),
  p2: require('../assets/Images/2.jpg'),
  p3: require('../assets/Images/3.jpeg'),
  p4: require('../assets/Images/4.jpeg'),
  p5: require('../assets/Images/5.jpeg'),
  p6: require('../assets/Images/8.jpg'),
  close: require('../assets/Images/close.png'),
  logoJoker: require('../assets/Images/joker.png'),
  refresh: require('../assets/Images/refresh.png'),
};
