import React, { Component } from "react";
import {
    View,
    Text,
    SafeAreaView,
    FlatList, Dimensions, TouchableOpacity, Image, Modal,
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Images } from '../../utils';
import Video from 'react-native-video';
import { WebView } from 'react-native-webview';
import axios from "axios";
const scalesPageToFit = Platform.OS === 'android';
const Data = [];
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            videos: [],
            // videourl: ''
        };
    }
    componentDidMount() {
        // this.setState({ videos:Data })
        this.getData()
    }
    getData() {
        axios.get('https://techiebirds.com/api/jokerbabu').then((response) => {
            // console.log(response.data)
            this.setState({ videos: response.data })
        })
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ width: width * 1, backgroundColor: '#000', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Image source={Images.logoJoker} style={{ width: width * 0.6, height: height * 0.07, marginTop: 5, marginBottom: 10, marginLeft: 10 }} />
                    <TouchableOpacity onPress={() => this.props.navigation.replace('Home')} >
                        <Image source={Images.refresh} style={[styles.closeIcon, { marginRight: 20 }]} />
                    </TouchableOpacity>
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ marginTop: '5%', justifyContent: 'center', alignItems: 'center', paddingBottom: '20%' }}
                    // horizontal={true}
                    data={this.state.videos}
                    numColumns={2}
                    renderItem={({ item }) => (
                        <>
                            <TouchableOpacity style={styles.tempCard} onPress={() => { console.log(item.url); this.setState({ show: true, videourl: item.url }) }}
                            >
                                <Image source={{ uri: item.thumb }} style={styles.thumbStyle} />
                                <Text numberOfLines={2} style={{ marginTop: '5%', color: '#3333ff' }}>{item.title}</Text>
                            </TouchableOpacity>
                        </>
                    )}
                    keyExtractor={item => item.url}
                />
                <Modal
                    // transparent={true}
                    // style={{backgroundColor:'#000'}}
                    animationType='slide'
                    visible={this.state.show}
                >
                    <TouchableOpacity onPress={() => { this.setState({ show: false }) }} style={{ alignItems: 'flex-end', marginTop: '2%', marginRight: '5%' }}>
                        <Image source={Images.close} style={styles.closeIcon} />
                    </TouchableOpacity>
                    <WebView
                        scalesPageToFit={scalesPageToFit}
                        bounces={false}
                        scrollEnabled={false}
                        // source={{ uri: 'https://jokerbabu.com/funniest-and-scariest-compilation-6558' }}
                        source={{ uri: this.state.videourl }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        useWebKit={true}
                        allowsInlineMediaPlayback={true}
                        mediaPlaybackRequiresUserAction={false}
                        mixedContentMode='always'
                        originWhitelist={['*']}
                        style={{ backgroundColor: '#fff', height, marginTop: '3%' }}
                    />
                    {/* <View style={{ backgroundColor: '#000000', height: height * 1, justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => { this.setState({ show: false }) }} style={{ position: 'absolute', left: '80%', top: '5%' }}>
                            <Image source={Images.close} style={styles.closeIcon} />
                        </TouchableOpacity>
                        <Video
                            ref={(ref) => {
                                this.player = ref
                            }}
                            controls={false}
                            fullscreen={true}
                            // source={{ uri: this.state.videourl }}   // Can be a URL or a local file.
                            source={{ uri: 'https://jokerbabu.com/wp-content/uploads/videos/100%20Prank%20Videos/Untitled%2019_720p.mp4' }}
                            resizeMode={'contain'}
                            // source={{uri:'https://www.youtube.com/watch?v=W0Lf3mfRFoc'}} 
                            style={{ width: 500, height: 300 }}
                        />
                    </View> */}
                </Modal>
                {/* Modal END */}
            </SafeAreaView>
        );
    }
}
export default Home;
