import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        display: 'flex',
        // backgroundColor: "#000"
    },
    tempCard: {
        width: width * 0.47,
        height: width * 0.58,
        borderRadius: 10,
        // margin: 6,
        marginLeft:6,
        marginRight:6,
        // marginTop:6,
        marginBottom:5,
        justifyContent: 'center',
        alignItems: 'center',
        // elevation:0.5,
        // flexDirection: 'row'
        // paddingBottom:"10%"
    },
    thumbStyle: {
        width: width * 0.45,
        height: width * 0.45,
        borderRadius: 10,
    },
    testModel: {
        backgroundColor: '#FFFFFF',
        marginTop: height * 0.3,
        marginLeft: 30,
        marginRight: 30,
        marginBottom: height * 0.2,
        borderRadius: 10,
        flex: 1,
        borderColor: "#29E093",
        // borderWidth: 1
    },
    middleWrapper1: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#006631',
        // paddingLeft: 15
    },
    nameWrapper: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        
      },
      closeIcon: {
        height: height * 0.035,
        width: height * 0.035,
        marginLeft: width * 0.04,
    },
});
